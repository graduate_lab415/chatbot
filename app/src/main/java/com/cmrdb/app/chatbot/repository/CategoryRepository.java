package com.cmrdb.app.chatbot.repository;

import android.app.Application;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cmrdb.app.chatbot.utils.Category;
import com.cmrdb.app.chatbot.viewmodel.callback.CategoryVolleyCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CategoryRepository {

    private static final String TAG = "ChatRepository";

    Application mApplication;
    RequestQueue mRQ;

    public CategoryRepository(Application application) {
        mApplication = application;
        mRQ = Volley.newRequestQueue(mApplication);
    }

    /**
     * 呼叫「取得類別」API
     */
    public void get(CategoryVolleyCallback callback) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                "http://120.110.112.177:83/Chatbot/getCategories.php",
                response -> {
                    Log.d(TAG, "onResponse: " + response);
                    ArrayList<Category> categoryList = new ArrayList<>();
                    try {
                        JSONObject json = new JSONObject(response);
                        JSONArray categories = json.getJSONArray("categories");

                        for (int i = 0; i < categories.length(); i++) {
                            JSONObject category = categories.getJSONObject(i);
                            categoryList.add(new Category(
                                    category.getString("id"),
                                    category.getString("name")));
                        }

                        callback.onSuccess(categoryList);
                    } catch (JSONException e) {
                        callback.onFail("發生一些錯誤\n" + e.getLocalizedMessage());
                    }

                },
                error -> {
                    Log.e(TAG, "onErrorResponse: ", error);
                    callback.onFail("發生一些錯誤\n" + error.toString());
                });
        mRQ.add(stringRequest);
    }
}
