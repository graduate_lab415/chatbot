package com.cmrdb.app.chatbot.viewmodel.callback;

import com.cmrdb.app.chatbot.utils.Option;

/**
 * 回傳選項被點擊狀況的介面
 */
public interface OptionClickCallback {
    void onOptionClick(Option option);
    void noneOfAbove(Option option);
}
