package com.cmrdb.app.chatbot.viewmodel;

import android.app.AlertDialog;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.cmrdb.app.chatbot.repository.CategoryRepository;
import com.cmrdb.app.chatbot.utils.Category;
import com.cmrdb.app.chatbot.utils.Chat;
import com.cmrdb.app.chatbot.viewmodel.callback.CategoryVolleyCallback;
import com.cmrdb.app.chatbot.viewmodel.callback.MyVolleyCallback;

import java.util.ArrayList;

public class CategoryViewModel extends AndroidViewModel {

    private static final String TAG = "CategoryViewModel";

    private final MutableLiveData<ArrayList<Category>> mCategoryLiveData;
    private final ArrayList<Chat> mCategoryArrayList;
    private final Application mApplication;

    public CategoryViewModel(@NonNull Application application) {
        super(application);
        mApplication = application;
        mCategoryLiveData = new MutableLiveData<>();
        mCategoryArrayList = new ArrayList<>();
        show();
    }

    public MutableLiveData<ArrayList<Category>> getMutableLiveData() {
        return mCategoryLiveData;
    }

    /**
     * 取得所有類別
     */
    public void show() {
        CategoryRepository repository = new CategoryRepository(mApplication);
        repository.get(new CategoryVolleyCallback() {
            @Override
            public void onSuccess(ArrayList<Category> categories) {
                mCategoryLiveData.setValue(categories);
            }

            @Override
            public void onFail(String message) {
                new AlertDialog.Builder(mApplication)
                        .setTitle("發生錯誤")
                        .setMessage(message)
                        .setPositiveButton("好", null)
                        .show();
            }
        });
    }
}
