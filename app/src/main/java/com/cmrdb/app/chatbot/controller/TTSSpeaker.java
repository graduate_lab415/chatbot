package com.cmrdb.app.chatbot.controller;

import android.app.Application;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import com.cmrdb.app.chatbot.viewmodel.callback.TTSCallback;

import java.util.Locale;

public class TTSSpeaker {
    private TextToSpeech mTTS;
    private TTSCallback mCallback;
    private Application mApplication;

    public TTSSpeaker(Application application, TTSCallback callback, UtteranceProgressListener utteranceProgressListener) {
        mApplication = application;
        mCallback = callback;
        mTTS = new TextToSpeech(mApplication, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = mTTS.setLanguage(Locale.TAIWAN);    //設定語言為中文
                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    } else {
                        mTTS.setPitch(1);    //語調(1為正常語調；0.5比正常語調低一倍；2比正常語調高一倍)
                        mTTS.setSpeechRate(1);    //速度(1為正常速度；0.5比正常速度慢一倍；2比正常速度快一倍)
                    }
                    mTTS.setOnUtteranceProgressListener(utteranceProgressListener);
                    mCallback.onReady();
                } else {
                    Log.e("TTS", "Initialization Failed!");
                    mCallback.onFail();
                }
            }

        });
    }

    public void speak(String message) {
        mTTS.speak(message, TextToSpeech.QUEUE_FLUSH, null, "tts1");
//        destroy();
    }

    public void destroy() {
            mTTS.shutdown();
    }
}
