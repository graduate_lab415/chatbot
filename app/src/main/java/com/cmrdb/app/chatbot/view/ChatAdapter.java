package com.cmrdb.app.chatbot.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.cmrdb.app.chatbot.utils.Chat;
import com.cmrdb.app.chatbot.utils.Option;
import com.cmrdb.app.chatbot.viewmodel.callback.OptionClickCallback;
import com.cmrdb.app.chatbot.R;

import java.util.ArrayList;

/**
 * 橋接聊天紀錄
 * 控制聊天 item 中的元件
 */
public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private final ArrayList<Chat> mChatList;
    private final OptionClickCallback mOptionClickCallback;

    public ChatAdapter(Context context, ArrayList<Chat> chatList, OptionClickCallback optionClickCallback) {
        this.mContext = context;
        this.mChatList = chatList;
        this.mOptionClickCallback = optionClickCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(mContext).inflate(R.layout.chat_item, parent, false);
        return new RecyclerViewViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Chat chat = mChatList.get(position);
        RecyclerViewViewHolder viewHolder = (RecyclerViewViewHolder) holder;

        viewHolder.icon.setImageResource(chat.getIcon());
        viewHolder.text.setText(chat.getText());

        // 奇數列，底色改灰色
        if (position % 2 != 0) {
            viewHolder.constraintLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.list_background));
        }

        // 有選項的 Chat，才顯示 Option 區塊
        if (mChatList.get(position).getOptions().size() < 1) {
            viewHolder.optionsArea.setVisibility(View.GONE);
        } else {
            // 取得所有 Option
            ArrayList<Option> options = mChatList.get(position).getOptions();

            // 設置選項的文字
            viewHolder.opt1.setText(options.get(0).getQuestion());
            viewHolder.opt2.setText(options.get(1).getQuestion());
            viewHolder.opt3.setText(options.get(2).getQuestion());

            // 設置三個選項的監聽器
            viewHolder.opt1.setOnClickListener(v -> mOptionClickCallback.onOptionClick(options.get(0)));
            viewHolder.opt2.setOnClickListener(v -> mOptionClickCallback.onOptionClick(options.get(1)));
            viewHolder.opt3.setOnClickListener(v -> mOptionClickCallback.noneOfAbove(options.get(2)));

            // 選項顯示的開關，預設 Gone
            viewHolder.option_message.setOnClickListener(v -> {
                if (viewHolder.options.getVisibility() == View.GONE) {
                    viewHolder.options.setVisibility(View.VISIBLE);
                } else {
                    viewHolder.options.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mChatList.size();
    }

    private static class RecyclerViewViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView text, option_message;
        ConstraintLayout constraintLayout;
        LinearLayout optionsArea, options;
        Button opt1, opt2, opt3;

        public RecyclerViewViewHolder(@NonNull View itemView) {
            super(itemView);

            icon = itemView.findViewById(R.id.img);
            text = itemView.findViewById(R.id.text);
            option_message = itemView.findViewById(R.id.option_message);
            constraintLayout = itemView.findViewById(R.id.constraint_layout);
            optionsArea = itemView.findViewById(R.id.options_area);
            options = itemView.findViewById(R.id.options);
            opt1 = itemView.findViewById(R.id.option1);
            opt2 = itemView.findViewById(R.id.option2);
            opt3 = itemView.findViewById(R.id.option3);
        }
    }
}
