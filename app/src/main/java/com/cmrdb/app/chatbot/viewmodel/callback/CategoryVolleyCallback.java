package com.cmrdb.app.chatbot.viewmodel.callback;

import com.cmrdb.app.chatbot.utils.Category;
import com.cmrdb.app.chatbot.utils.Chat;

import java.util.ArrayList;

public interface CategoryVolleyCallback {
    void onSuccess(ArrayList<Category> categories);

    void onFail(String message);
}
