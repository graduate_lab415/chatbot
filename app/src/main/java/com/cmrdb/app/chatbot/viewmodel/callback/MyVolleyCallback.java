package com.cmrdb.app.chatbot.viewmodel.callback;

import com.cmrdb.app.chatbot.utils.Chat;

/**
 * 回傳 Volley 成功與否的介面
 */
public interface MyVolleyCallback {
    void onSuccess(Chat c);

    void onFail(Chat c);
}
