package com.cmrdb.app.chatbot.utils;

/**
 * 問題類別
 */
public class Category {
    private final String mId;
    private final String mName;

    public Category(String id, String name) {
        mId = id;
        mName = name;
    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }
}
