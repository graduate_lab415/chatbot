package com.cmrdb.app.chatbot.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cmrdb.app.chatbot.utils.Chat;
import com.cmrdb.app.chatbot.viewmodel.ChatViewModel;
import com.cmrdb.app.chatbot.utils.Option;
import com.cmrdb.app.chatbot.viewmodel.callback.OptionClickCallback;
import com.cmrdb.app.chatbot.R;

import java.util.ArrayList;
import java.util.Locale;

/**
 * 處理聊天頁面的畫面
 */
public class ChatActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private EditText mUserInput;
    private ChatViewModel mViewModel;
    private LinearLayout mProgressLayout;
    int REQUEST_CODE_STT = 1;
    String TAG = "ChatActivity";
    private String categoryId, categoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        // 取得從上一頁傳過來的分類 id 與名稱，並顯示在 Action Bar 上
        Intent intent = getIntent();
        categoryId = intent.getStringExtra("category_id");
        categoryName = intent.getStringExtra("category_name")
                .replace("\n", "");
        Log.d(TAG, "get from intent: " + categoryId + " " + categoryName);
        getSupportActionBar().setTitle("分類 > " + categoryName);

        mRecyclerView = findViewById(R.id.chat_recycle_view);
        mUserInput = findViewById(R.id.user_input);
        mProgressLayout = findViewById(R.id.progress_dialog_layout);

        mViewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
        mViewModel.getChatMutableLiveData().observe(this, mChatListUpdateObserver);

        Button mSubmit = findViewById(R.id.submit);
        View.OnClickListener clickListener =
                v -> {
                    if (!mUserInput.getText().toString().equals("")) {
                        mViewModel.sendQuery(mUserInput.getText().toString(), categoryId);
                        hideKeyboard();
                        mUserInput.setText("");
                        mProgressLayout.setVisibility(View.VISIBLE);
                    } else {
                        new AlertDialog.Builder(ChatActivity.this)
                                .setTitle(R.string.no_input_alert_title)
                                .setMessage(R.string.no_input_alert_msg)
                                .setPositiveButton("OK", null)
                                .show();
                    }
                };
        mSubmit.setOnClickListener(clickListener);


        ImageButton btnSpeak = findViewById(R.id.speak);
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sttIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                sttIntent.putExtra(
                        RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
                );
                sttIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                sttIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.start_to_speak));

                try {
                    startActivityForResult(sttIntent, REQUEST_CODE_STT);
                } catch (ActivityNotFoundException e) {
                    Log.d(TAG, "onClick: " + e.getLocalizedMessage());
                    Toast.makeText(ChatActivity.this, "Your device does not support STT.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private final Observer<ArrayList<Chat>> mChatListUpdateObserver = new Observer<ArrayList<Chat>>() {
        @Override
        public void onChanged(ArrayList<Chat> chatArrayList) {
            OptionClickCallback optionClickCallback = new OptionClickCallback() {
                @Override
                public void onOptionClick(Option option) {
                    mViewModel.sendOptionMessage(option, categoryId);
                }

                @Override
                public void noneOfAbove(Option option) {
                    mViewModel.sendOptionMessage(option, categoryId);
                }
            };
            ChatAdapter mChatAdapter = new ChatAdapter(ChatActivity.this, chatArrayList,
                    optionClickCallback);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
            mRecyclerView.setAdapter(mChatAdapter);
            mRecyclerView.scrollToPosition(mChatAdapter.getItemCount() - 1);
            mProgressLayout.setVisibility(View.GONE);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_STT) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                if (result != null) {
                    String recognizedText = result.get(0);
                    mUserInput.setText(recognizedText);
                }
            }
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}