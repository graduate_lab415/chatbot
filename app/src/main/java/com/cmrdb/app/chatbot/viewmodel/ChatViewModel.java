package com.cmrdb.app.chatbot.viewmodel;

import android.app.Application;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.cmrdb.app.chatbot.utils.Chat;
import com.cmrdb.app.chatbot.repository.ChatRepository;
import com.cmrdb.app.chatbot.viewmodel.callback.MyVolleyCallback;
import com.cmrdb.app.chatbot.utils.Option;
import com.cmrdb.app.chatbot.R;
import com.cmrdb.app.chatbot.viewmodel.callback.TTSCallback;
import com.cmrdb.app.chatbot.controller.TTSSpeaker;

import java.util.ArrayList;

/**
 * 處理聊天頁面的邏輯與流程
 */
public class ChatViewModel extends AndroidViewModel {

    private static final String TAG = "ChatViewModel";

    private final MutableLiveData<ArrayList<Chat>> mChatLiveData;
    private final ArrayList<Chat> mChatArrayList;
    private final Application mApplication;
    private TTSSpeaker mSpeaker;


    public ChatViewModel(@NonNull Application application) {
        super(application);
        mApplication = application;
        mChatLiveData = new MutableLiveData<>();
        mChatArrayList = new ArrayList<>();
        initChats();
    }

    public MutableLiveData<ArrayList<Chat>> getChatMutableLiveData() {
        return mChatLiveData;
    }

    /**
     * 初始聊天資料
     */
    private void initChats() {
        Chat chat = new Chat("Zenbo", R.drawable.zenbo, "嗨！你好，我是 Zenbo，你可以問我長照的問題！");
        showMessage(chat);
    }

    public void sendQuery(String message, String categoryId) {
        // 使用者輸入的句子
        Chat chat = new Chat("User", R.drawable.user, message);
        showMessage(chat);

        // Zenbo 的回覆
        ChatRepository repository = new ChatRepository(mApplication);
        repository.getAnswer(message, categoryId, new MyVolleyCallback() {
            @Override
            public void onSuccess(Chat c) {
                Log.d(TAG, "onSuccess");
                showMessage(c);
            }

            @Override
            public void onFail(Chat c) {
                Log.e(TAG, "onFail");
                showMessage(c);
            }
        });
    }

    public void sendOptionMessage(Option option, String categoryId) {
        Chat chat;
        if (option.getId() == -1) {
            chat = new Chat("Zenbo", R.drawable.zenbo,
                    "對不起，我聽不懂，請換一種方式詢問或換一個分類。我已經記下您的問題，我會持續優化系統。");
        } else {
            chat = new Chat("Zenbo", R.drawable.zenbo,
                    option.getQuestion() + "\n\n" + option.getAnswer());
        }
        showMessage(chat);

        ChatRepository repository = new ChatRepository(mApplication);
        repository.addAdjustment(option.getQuery(), option.getId(), categoryId);
    }

    private void showMessage(Chat c) {
        mChatArrayList.add(c);
        mChatLiveData.setValue(mChatArrayList);
        if (c.getName().equals("Zenbo")) speak(c.getText());
    }

    private void speak(String message) {
        TTSCallback ttsCallback = new TTSCallback() {
            @Override
            public void onReady() {
                mSpeaker.speak(message);
            }

            @Override
            public void onFail() {
                Log.d(TAG, "ttsCallback onFail");
            }
        };
        UtteranceProgressListener speakingStatus = new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                Log.d(TAG, "speakingStatus onStart");
            }

            @Override
            public void onDone(String utteranceId) {
                Log.d(TAG, "speakingStatus onDone");
                mSpeaker.destroy();
            }

            @Override
            public void onError(String utteranceId) {
                Log.d(TAG, "speakingStatus onError");
                mSpeaker.destroy();
            }
        };

        mSpeaker = new TTSSpeaker(mApplication, ttsCallback, speakingStatus);
    }
}
