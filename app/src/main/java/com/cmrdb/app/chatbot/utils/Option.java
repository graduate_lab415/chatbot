package com.cmrdb.app.chatbot.utils;

import java.util.HashMap;

/**
 * 其他選項
 */
public class Option {
    private String mQuery;
    private int mQAId;
    private String mQuestion;
    private String mAnswer;

    public Option(String query) {
        this(query, -1, "", "");
    }

    public Option(String query, int qaId, String question, String answer) {
        this.mQuery = query;
        this.mQAId = qaId;
        this.mQuestion = question;
        this.mAnswer = answer;
    }

    public String getQuery() {
        return mQuery;
    }

    public int getId() {
        return mQAId;
    }

    public String getQuestion() {
        return mQuestion;
    }

    public String getAnswer() {
        return mAnswer;
    }

    @Override
    public String toString() {
        HashMap<String, String> map = new HashMap<>();
        map.put("query", mQuery);
        map.put("qaId", Integer.toString(mQAId));
        map.put("question", mQuestion);
        map.put("answer", mAnswer);
        return map.toString();
    }
}
