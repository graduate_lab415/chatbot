package com.cmrdb.app.chatbot.viewmodel.callback;

public interface TTSCallback {
    void onReady();
    void onFail();
}
