package com.cmrdb.app.chatbot.repository;

import android.app.Application;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cmrdb.app.chatbot.utils.Chat;
import com.cmrdb.app.chatbot.viewmodel.callback.MyVolleyCallback;
import com.cmrdb.app.chatbot.utils.Option;
import com.cmrdb.app.chatbot.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 聊天相關，與 API 溝通的部分
 */
public class ChatRepository {

    private static final String TAG = "ChatRepository";

    Application mApplication;
    RequestQueue mRQ;

    public ChatRepository(Application application) {
        mApplication = application;
        mRQ = Volley.newRequestQueue(mApplication);
    }

    public void getAnswer(String query, String categoryId, MyVolleyCallback callback) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                "http://120.110.112.177:83/Chatbot/getAnswer.php",
                response -> {
                    Log.d(TAG, "onResponse: " + response);
                    Chat answer;
                    try {
                        JSONObject json = new JSONObject(response);
                        JSONArray data = json.getJSONArray("data");
                        JSONObject bestOption = data.getJSONObject(0);
                        String q = bestOption.getString("Q");
                        String a = bestOption.getString("A");

                        ArrayList<Option> options = new ArrayList<>();
                        JSONObject opt1 = data.getJSONObject(1);
                        Option option1 = new Option(query,
                                opt1.getInt("id"),
                                opt1.getString("Q"),
                                opt1.getString("A"));
                        options.add(option1);
                        JSONObject opt2 = data.getJSONObject(2);
                        Option option2 = new Option(query,
                                opt2.getInt("id"),
                                opt2.getString("Q"),
                                opt2.getString("A"));
                        options.add(option2);
                        Option optionNoneOfAbove = new Option(query, -1, "以上皆非", "");
                        options.add(optionNoneOfAbove);

                        answer = new Chat("Zenbo", R.drawable.zenbo, q + "\n\n" + a, options);
                    } catch (JSONException e) {
                        answer = new Chat("Zenbo", R.drawable.zenbo, "發生一些錯誤\n" + e.getLocalizedMessage());
                        callback.onFail(answer);
                    }
                    callback.onSuccess(answer);
                },
                error -> {
                    Log.e(TAG, "onErrorResponse: ", error);
                    Chat answer = new Chat("Zenbo", R.drawable.zenbo, "發生一些錯誤\n" + error.toString());
                    callback.onFail(answer);
                }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> map = new HashMap<>();
                map.put("q", query);
                map.put("category_id", categoryId);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MyVolleyConfig.DEFAULT_TIMEOUT_MS,
                MyVolleyConfig.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRQ.add(stringRequest);
    }

    public void addAdjustment(String query, int qaId, String categoryId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                "http://120.110.112.177:83/Chatbot/addAdjustment.php",
                response -> Log.d(TAG, "addAdjustment onResponse: " + response),
                error -> Log.e(TAG, "addAdjustment onErrorResponse: " + error.getLocalizedMessage())) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> map = new HashMap<>();
                map.put("q", query);
                map.put("qa_id", qaId + "");
                map.put("category_id", categoryId);
                return map;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MyVolleyConfig.DEFAULT_TIMEOUT_MS,
                MyVolleyConfig.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRQ.add(stringRequest);
    }
}
