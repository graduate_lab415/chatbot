package com.cmrdb.app.chatbot.repository;

/**
 * Volley 相關的參數
 */
public class MyVolleyConfig {
    // 實測平均回傳時間 5~8 秒，timeout 15 應該足夠
    public static final int DEFAULT_TIMEOUT_MS = 15 * 1000;
    public static final int DEFAULT_MAX_RETRIES = 1;
}
