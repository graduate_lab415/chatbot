package com.cmrdb.app.chatbot.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.cmrdb.app.chatbot.R;
import com.cmrdb.app.chatbot.utils.Category;
import com.cmrdb.app.chatbot.viewmodel.CategoryViewModel;

import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity {

    private static final String TAG = "CategoryActivity";

    private CategoryViewModel mViewModel;
    private Button[] buttons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);
        mViewModel.getMutableLiveData().observe(this, mCategoryUpdateObserver);

        buttons = new Button[]{
                findViewById(R.id.btn1),
                findViewById(R.id.btn2),
                findViewById(R.id.btn3),
                findViewById(R.id.btn4),
                findViewById(R.id.btn5),
                findViewById(R.id.btn6),
        };
    }

    /**
     * 為按鈕設置監聽器
     * 點到之後會換到對話頁
     */
    private void setBtnClickListener(Button btn, Category category) {
        View.OnClickListener mBtnClicked = v -> {
            Intent intent = new Intent(CategoryActivity.this, ChatActivity.class);
            intent.putExtra("category_id", category.getId());
            intent.putExtra("category_name", category.getName());
            startActivity(intent);
        };
        btn.setOnClickListener(mBtnClicked);
    }

    private final Observer<ArrayList<Category>> mCategoryUpdateObserver = new Observer<ArrayList<Category>>() {
        @Override
        public void onChanged(ArrayList<Category> categories) {
            for (int i = 0; i < buttons.length; i++) {
                Log.d(TAG, "onChanged: " + buttons[i] + " " + categories.get(i));
                buttons[i].setText(categories.get(i).getName());
                setBtnClickListener(buttons[i], categories.get(i));
            }
        }
    };
}