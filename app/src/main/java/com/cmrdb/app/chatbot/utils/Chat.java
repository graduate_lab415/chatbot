package com.cmrdb.app.chatbot.utils;

import com.cmrdb.app.chatbot.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 聊天紀錄
 */
public class Chat {
    /**
     * 講話者名稱
     */
    private String mName;
    /**
     * 講話者圖示
     */
    private int mIcon;
    /**
     * 講話內容
     */
    private String mText;
    /**
     * 這句對話的其他選項
     */
    private ArrayList<Option> mOptions;

    public Chat() {
        this("", R.drawable.ic_launcher_background, "text", new ArrayList<>());
    }

    public Chat(String name, int icon, String text) {
        this(name, icon, text, new ArrayList<>());
    }

    public Chat(String name, int icon, String text, ArrayList<Option> options) {
        this.mName = name;
        this.mIcon = icon;
        this.mText = text;
        this.mOptions = options;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getIcon() {
        return mIcon;
    }

    public void setIcon(int icon) {
        this.mIcon = icon;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        this.mText = text;
    }

    public ArrayList<Option> getOptions() {
        return mOptions;
    }

    public void setOptions(ArrayList<Option> options) {
        this.mOptions = options;
    }

    @Override
    public String toString() {
        HashMap<String, String> map = new HashMap<>();
        map.put("name", mName);
        map.put("icon", mIcon + "");
        map.put("text", mText);
        map.put("options", mOptions.toString());
        return map.toString();
    }
}
